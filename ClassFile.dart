class App {
  String Name = "";
  String Category = "";
  String Developer = "";
  String Year = "";

  App(Map<String, String> init) {
    Name = init['Name'] as String;
    Category = init['Category'] as String;
    Developer = init['Developer'] as String;
    Year = init['Year'] as String;
  }

  capitalize() {
    return Name.toUpperCase();
  }

  void showAppInfo() {
    print('Category:${Category}');
    print('Developer:${Developer}');
    print('Year:${Year}');
  }

}

void main() {
  List apps = [
    {
      'Name': 'Fnb banking',
      'Category': 'Best Overall App, Best IOS Consumer App',
      'Developer': 'FirstRand Bank Limited',
      'Year': '2012',
    },
    {
      'Name': 'SnapScan',
      'Category': 'Best HTML App',
      'Developer': 'Co-founder-Kobus Ehlers',
      'Year': '2013',
    },
    {
      'Name': 'Zapper',
      'Category': 'Consumer Choice award winner',
      'Developer': 'David de Villers',
      'Year': '2014',
    },
    {
      'Name': 'Dstv Now',
      'Category': 'Best Consumer App',
      'Developer': 'MultiChoice South Africa',
      'Year': '2015',
    },
    {
      'Name': 'HearZA',
      'Category': 'Best Enterprise Development App',
      'Developer': 'HearX Group',
      'Year': '2016',
    },
    {
      'Name': 'Shyft',
      'Category': 'Best Overall App',
      'Developer': 'Standard Bank',
      'Year': '2017',
    },
    {
      'Name': 'Khula',
      'Category': 'Best AgriCultural Solution',
      'Developer': 'Co-founders-Karidas Tshintsholo and Matthew Piper',
      'Year': '2018',
    },
    {
      'Name': 'Naked Insurance',
      'Category': 'Best Finacial Solution',
      'Developer': 'Co-founder-Ernest North',
      'Year': '2019',
    },
    {
      'Name': 'Bottles',
      'Category': 'Best South African Solution',
      'Developer': 'Co-founder-Vincent Viviers',
      'Year': '2020',
    },
    {
      'Name': 'Ambani-Afrika',
      'Category': 'Best Overall App',
      'Developer': 'Mukundi Lambani',
      'Year': '2021',
    },
  ];

  for (var i in apps) {
    var ap = new App(i);
    print(ap.capitalize());
    ap.showAppInfo();
  }
}
